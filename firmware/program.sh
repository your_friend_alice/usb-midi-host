#!/usr/bin/env bash
set -e
arduino-cli compile --fqbn forkedArduinoCore:samd:mattairtech_mt_d21e_revb --board-options clock=crystal_hs,cpu=samd21e16a,bootloader=0kb,serial=four_uart . -v
arduino-cli upload  --fqbn forkedArduinoCore:samd:mattairtech_mt_d21e_revb --board-options clock=crystal_hs,cpu=samd21e16a,bootloader=0kb,serial=four_uart . --programmer atmel_ice -v

