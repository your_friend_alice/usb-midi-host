#!/bin/bash
if [[ -z "$1" ]]; then
    tty=/dev/ttyACM0
else
    tty="$1"
fi
stty -F "$tty" 115200
cat "$tty"
