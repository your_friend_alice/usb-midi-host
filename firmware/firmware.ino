/*
 *******************************************************************************
 * USB-MIDI to Legacy Serial MIDI converter
 * Copyright (C) 2012-2017 Yuuichi Akagawa
 *
 * Idea from LPK25 USB-MIDI to Serial MIDI converter
 *     by Collin Cunningham - makezine.com, narbotic.com
 *
 * This is sample program. Do not expect perfect behavior.
 *******************************************************************************
 */

#include <usbh_midi.h>
#include <usbhub.h>
#include <Arduino.h>
#include "wiring_private.h"

#define USB_MIDI_HOST_DEBUG

USBHost UsbH;
USBH_MIDI Midi(&UsbH);

void MIDI_poll();
void doDelay(uint32_t t1, uint32_t t2, uint32_t delayTime);
unsigned int notes_on;
int blink_time = 10000;

void setup()
{
    Serial3.begin(31250);
    Serial1.begin(115200);
    // MIDI: PA0 SERCOM_ALT mode: SERCOM 1, PAD 0 - Serial3
    // Debug TX: PA04 SERCOM_ALT mode: SERCOM 0, pad 0 - Serial1
    // Debug RX: PA05 SERCOM_ALT mode: SERCOM 0, pad 1 - Serial1
    if (UsbH.Init()) {
        while (1); //halt
    }
    notes_on=0;
    pinMode(3, OUTPUT);
    delay(200);
}

void loop()
{
    UsbH.Task();
    uint32_t t1 = (uint32_t)micros();
    if ( Midi ) {
        MIDI_poll();
    }
    //delay(1ms)
    //doDelay(t1, (uint32_t) micros(), 1000);
}

void print_byte(uint8_t in) {
    uint8_t out = in >> 4 & 0x0f;
    if(out <= 9) {
        Serial1.write(out+48);
    } else {
        Serial1.write(out+55);
    }
    out = in & 0x0f;
    if(out <= 9) {
        Serial1.write(out+48);
    } else {
        Serial1.write(out+55);
    }
}

// Poll USB MIDI Controler and send to serial MIDI
void MIDI_poll()
{
    uint8_t outBuf[ 3 ];
    uint8_t size;
    uint8_t blank[]={0, 0, 0, 0, 0};

    do {
        if ( (size = Midi.RecvData(outBuf)) > 0) {
            if((outBuf[0] & 0xf0) == 0x90) {
                notes_on++;
            } else if((outBuf[0] & 0xf0) == 0x80) {
                notes_on--;
            }
            Serial3.write(outBuf, size);
            #ifdef USB_MIDI_HOST_DEBUG
            Serial1.print("MIDI Message: #");
            for(int i=0; i<size; i++) {
                print_byte(outBuf[i]);
            }
            Serial1.print(", ");
            Serial1.print(notes_on, DEC);
            Serial1.println("");
            #endif
        }
    } while (size > 0);
    digitalWrite(3, notes_on == 0);
}

// Delay time (max 16383 us)
void doDelay(uint32_t t1, uint32_t t2, uint32_t delayTime)
{
    uint32_t t3;
        t3 = t2 - t1;
    if ( t3 < delayTime ) {
        delayMicroseconds(delayTime - t3);
    }
}
