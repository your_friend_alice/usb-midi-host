pcb = [55, 20, 1.6];
pcb_r = 1;
screw_y_spacing = 13;
screw_x_pos = 34.5;
screw_d = 3.5;
screw_head_d = 8;
screw_pad_d = 6.2;
screw_insert_d = 4.8;
screw_insert_h = 5;
// inset support ring that can go all the way around the underside of the board
pcb_min_ring = 0.6;
pcb_west_support = 10;
bottom_relief_h = 2.5;
bottom_relief_r = 0.5;
// [x_center, y_center, x_size, y_size]
regular_bottom_reliefs = [
    [15, 15.5, 10, 8],
    [19.5, 1.75, 4.5, 2.25],
    [41.25, 10, 5, 17.5],
    [50.5, 10, 4, 19],
];
debug_bottom_reliefs = [
    [26, 0, 12, 39]
];
horizontal_wall = 0.8;
vertical_wall = 1.2;
min_vertical_wall = 0.8;
wiggle = 0.4;

midi_h = 21.8;
midi_flange_w = 1.8;
midi_flange_depth = 3;
midi_inset = 1;
midi_depth = 16;
midi_w = 21;

misc_smd_h = 2;

usba_h = 13.9;
usba_depth = 20.5;
usba_flange_w = 0.5;
usba_flange_depth = 1;
usba_y_pos = 15.5;
usba_w = 6;
usba_bulge_w = 1.5;
usba_bulge_depth = 6;

usbc_w = 9.5;
usbc_h = 3.3;
usbc_depth = 7.5;
usbc_y_pos = 6;

led_pos = [37, 8.2];
led_pipe_d = 2.5;
led_cap_h = 0.4;
led_hole_d = 1.5;

top = true;
bottom = true;
debug = false;

$fn=20;

h=midi_h;

module at_screws() for(y=[0,1]) translate([screw_x_pos, pcb[1]/2]) mirror([0, y, 0]) translate([0, screw_y_spacing/2, 0]) children();

module bottom_reliefs(debug) {
    //items = debug ? concat(regular_bottom_reliefs, debug_bottom_reliefs) : regular_bottom_reliefs;
    items = regular_bottom_reliefs;
    offset(r=bottom_relief_r) offset(delta=-bottom_relief_r) for(relief = items) {
        translate([relief[0], relief[1]]) square([relief[2], relief[3]], center=true);
    }
}
module pcb() difference() {
    union() {
        offset(r=pcb_r) offset(delta=-pcb_r) square([pcb[0], pcb[1]]);
        translate([pcb_r, 0]) square([pcb[0] - pcb_r, pcb[1]]);
    }
    at_screws() circle(d=screw_d);
}

module top_outline() hull() offset(r=vertical_wall) offset(delta=wiggle) intersection() {
    pcb();
    square([pcb[0] - vertical_wall + min_vertical_wall, pcb[1]]);
}

module top() {
    module cuts() {
        translate([pcb[0], midi_flange_w]) cube([999, pcb[1] - midi_flange_w*2, midi_h - midi_flange_w]);
        mirror([1, 0, 0]) {
            translate([-usba_depth, usba_y_pos - usba_w/2, 0]) cube([999, usba_w, usba_h]);
            translate([0, usba_y_pos - usba_w/2 - usba_flange_w, -999]) cube([usba_flange_depth, usba_w + usba_flange_w*2, 999 + usba_h + usba_flange_w]);
            translate([0, usba_y_pos - usba_w/2 - usba_bulge_w, -999]) mirror([1, 0, 0]) cube([usba_bulge_depth, usba_w + usba_bulge_w*2, 999 + usba_h]);
        }
        // usbc hole
        translate([usbc_depth, usbc_y_pos - usbc_w/2, 0]) mirror([1, 0, 0])cube([999, usbc_w, usbc_h]);
        // insert holes
        at_screws() cylinder(d=screw_insert_d, h=h);
        // led hole
        translate(led_pos) cylinder(d=led_pipe_d, h=h + horizontal_wall - led_cap_h - horizontal_wall);
        // midi flange
        translate([pcb[0]-midi_flange_w, (pcb[1]-midi_w)/2 - wiggle, -999]) cube([midi_flange_w + wiggle, midi_w + wiggle*2, h+999]);
    }
    module screw_nubs() translate([wiggle, wiggle]) at_screws() hull() for(y=[0,1]) translate([0, y*999, 0]) cylinder(d=screw_pad_d, h=h);
    difference() {
        union() {
            translate([0, 0, - pcb[2] - horizontal_wall - bottom_relief_h]) linear_extrude(h + pcb[2] + horizontal_wall + bottom_relief_h) difference() {
                top_outline();
                hull() offset(delta=wiggle) pcb();
            }
            translate([0, 0, h]) linear_extrude(horizontal_wall) top_outline();
            intersection() {
                linear_extrude(h) top_outline();
                translate([0, 0, h]) mirror([0, 0, 1]) {
                    translate([-wiggle, -wiggle, 0]) {
                        // midi top buffer
                        cube([pcb[0] + wiggle - midi_flange_w, pcb[1] + wiggle*2, midi_flange_depth]);
                        // middle fill
                        translate([wiggle + usba_depth, 0]) cube([pcb[0] - usba_depth - midi_depth , pcb[1] + wiggle*2, h - misc_smd_h]);
                        // screw nubs
                        screw_nubs();
                        // usba top buffer
                        cube([wiggle + usba_depth, pcb[1] + wiggle*2, h - usba_h]);
                        // usba side buffer
                        translate([0, pcb[1] + wiggle*2, 0]) mirror([0, 1, 0]) cube([usba_depth + wiggle, usba_w + wiggle, h]);
                        // usb side fill
                        cube([usba_depth + wiggle, pcb[1] + wiggle*2, h - misc_smd_h]);
                        // usbc top + side buffer
                        cube([usbc_depth + wiggle, pcb[1] + wiggle*2, h]);
                    }
                }
            }
        }
        difference() {
            union() {
                cuts();
                translate([0, 0, -pcb[2] - bottom_relief_h - horizontal_wall]) linear_extrude(bottom_relief_h + horizontal_wall) bottom_reliefs(true);
                linear_extrude(h) {
                    offset(r=pcb_r) offset(r=-vertical_wall-pcb_r) difference() {
                        translate([usba_depth, 0]) square([pcb[0] - usba_depth - midi_depth, pcb[1]]);
                        allowedCut();
                    }
                    offset(r=pcb_r) offset(r=-vertical_wall-pcb_r) difference() {
                        square([usba_depth + vertical_wall, pcb[1]]);
                        allowedCut();
                    }
                    offset(r=pcb_r) offset(r=-vertical_wall-pcb_r) difference() {
                        translate([pcb[0] - midi_depth, 0]) square([midi_depth - midi_flange_w, pcb[1]]);
                        allowedCut();
                    }
                    offset(r=pcb_r) offset(r=-vertical_wall-pcb_r) translate([-vertical_wall, usbc_y_pos - usbc_w/2, 0]) square([usbc_depth + vertical_wall*1.5, usbc_w]);

                    offset(r=pcb_r) offset(r=-vertical_wall-pcb_r) difference() {
                        translate([pcb[0] - midi_depth, 0]) square([midi_depth, pcb[1]]);
                        allowedCut();
                    }
                }
            }
            topCut(horizontal_wall);
        }
        translate(led_pos) cylinder(d=led_hole_d, h=h + horizontal_wall - led_cap_h);
        difference() {
            topCut(0);
            hull() for(x=[0,1]) translate([led_pos[0] + x*999, led_pos[1]]) cylinder(d=led_pipe_d + vertical_wall*2, h=999);
        }
    }
    module allowedCut() projection() {
        cuts();
        translate([-wiggle, -wiggle]) screw_nubs();
    }
    module topCut(offset) {
        r = pcb_r + vertical_wall;
        difference() {
            rotate([90, 0, 0]) linear_extrude(999, center=true) offset(r=-r) offset(delta=r + offset) polygon([
                [-999, usbc_h+horizontal_wall],
                [pcb[0] - midi_depth - vertical_wall - (midi_h - usbc_h), usbc_h+horizontal_wall],
                [pcb[0] - midi_depth - vertical_wall, h+horizontal_wall],
                [999, h+horizontal_wall],
                [999, 999],
                [-999, 999],
            ]);
            linear_extrude(usba_h + usba_flange_depth + horizontal_wall - offset) translate([offset==0 ? -vertical_wall - wiggle : -999/2, usba_y_pos - usba_w/2 - usba_flange_depth - vertical_wall, 0]) offset(r=pcb_r) offset(delta=-offset - pcb_r) square(999);
        }
    }
}

module bottom() {
    module m() offset(delta=-wiggle) difference() {
        top_outline();
        projection(cut=true) translate([0, 0, pcb[2]]) top();
    }
    difference() {
        union() {
            linear_extrude(horizontal_wall + bottom_relief_h - wiggle) m();
            linear_extrude(horizontal_wall + bottom_relief_h) pcb();
        }
        translate([0, 0, horizontal_wall]) linear_extrude(999) bottom_reliefs(debug);
        at_screws() translate([0, 0, -1]) {
            cylinder(d1=screw_head_d + 2, d2=0, h=screw_head_d/2 + 1);
            cylinder(d=screw_d, h=999);
        }
    }
}
if(top) top();
if(bottom) translate([0, 0, -horizontal_wall - bottom_relief_h - pcb[2]]) bottom();
