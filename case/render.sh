#!/bin/bash
trap 'trap - SIGTERM && kill 0' SIGINT SIGTERM EXIT
dir="$(dirname "$0")"
fn=90
model="$dir/model.scad"
(
    set -ex;
    time openscad -D "\$fn=$fn" -D top=true -D bottom=false -o "$dir/top-transparent.stl" "$model" &
    time openscad -D "\$fn=$fn" -D top=true -D bottom=false -D led_cap_h=0 -o "$dir/top-opaque.stl" "$model" &
    time openscad -D "\$fn=$fn" -D top=false -D bottom=true -o "$dir/bottom.stl" "$model" &
    wait
)
